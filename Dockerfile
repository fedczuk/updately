FROM python:3-alpine

RUN mkdir -p /opt/app
COPY . /opt/app
WORKDIR /opt/app

RUN pip install -r requirements.txt