#!/bin/bash

OUTDATED=`pip list --outdated --format=columns`
PACKAGE_NAMES=`awk -F"(==|<|>)" '{print $1}' requirements.txt`

UPDATES=`echo "${OUTDATED}" | grep -Fwi -e "${PACKAGE_NAMES}"`

if [[ -n "${UPDATES}" ]]; then
    echo "${OUTDATED}" | head -n 2
    echo "${UPDATES}"

    exit 1
else
    echo "THERE IS NO PACKAGES TO UPDATE"
fi